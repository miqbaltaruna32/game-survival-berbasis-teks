package com.konsumsi;

public class Minuman{
    private String namaMinuman;
    private boolean penuh, aman;
    private int efekAir, efekKesehatan;

    public Minuman(boolean penuh, boolean aman){
        this.penuh = penuh;
        this.aman = aman;
        if(penuh){
            if(aman) {
                namaMinuman = "Air Botol(Bersih)";
                efekAir = 25;
                efekKesehatan = 0;
            }
            else{
                namaMinuman = "Air Botol(Kotor)";
                efekAir = 10;
                efekKesehatan = -10;
            }
        } else {
            namaMinuman = "Air Botol(Kosong)";
            this.aman = false;
            efekAir = 0;
        }
    }

    // Method
    public void kosongkanBotol(){
        namaMinuman = "Air Botol(Kosong)";
        penuh = false;
        aman = false;
        efekAir = 0;
    }
    public void isiBotol(){
        namaMinuman = "Air Botol(Kotor)";
        penuh = true;
        aman = false;
        efekAir = 10;
        efekKesehatan = -10;
    }
    public void diRebus(){
        namaMinuman = "Air Botol(Bersih)";
        penuh = true;
        aman = true;
        efekAir = 25;
        efekKesehatan = 0;
    }

    // Getter
    public String getNamaMinuman(){
        return namaMinuman;
    }
    public boolean isPenuh() {
        return penuh;
    }
    public boolean isAman() {
        return aman;
    }
    public int getEfekAir() {
        return efekAir;
    }
    public int getEfekKesehatan() {
        return efekKesehatan;
    }
    // Setter

}
