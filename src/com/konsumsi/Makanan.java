package com.konsumsi;
import com.main.Time;

public class Makanan{
    private String namaMakanan;
    private int efekAir, efekMakanan, efekKesehatan;
    private boolean mentah, busuk, bisaDimasak;
    private Time kesegaran;

    public Makanan(){}

    // Constructor
    public Makanan(String namaMakanan, int efekMakanan, int efekKesehatan, int kesegaran){
        this.namaMakanan = namaMakanan;
        this.efekMakanan = efekMakanan;
        this.efekKesehatan = efekKesehatan;
        this.kesegaran = new Time(kesegaran);
        bisaDimasak = false;
    }

    // Method Biasa
    public void pembusukanMakanan(){
        if(getEfekAir() > 0)
            setEfekAir(-5);
        setBusuk(true);
        setNamaMakanan(getNamaMakanan() + " (Busuk)");
        setEfekMakanan(getEfekMakanan() / 2);
        setEfekKesehatan(-20);
    }
    public void dimasak(){}
    public void kurangiKesegaran(int waktu){
        kesegaran.kurangiWaktu(waktu);
    }

    // Getter
    public String getNamaMakanan() {
        return namaMakanan;
    }
    public int getEfekMakanan() {
        return efekMakanan;
    }
    public int getEfekKesehatan() {
        return efekKesehatan;
    }
    public int getEfekAir() {
        return efekAir;
    }

    public boolean isMentah() {
        return mentah;
    }
    public boolean isBusuk() {
        return busuk;
    }
    public boolean isBisaDimasak(){
        return bisaDimasak;
    }
    public int getKesegaran(){
        return kesegaran.getWaktuUniversal();
    }

    // Setter

    public void setNamaMakanan(String namaMakanan) {
        this.namaMakanan = namaMakanan;
    }
    public void setEfekMakanan(int efekMakanan) {
        this.efekMakanan = efekMakanan;
    }
    public void setEfekKesehatan(int efekKesehatan) {
        this.efekKesehatan = efekKesehatan;
    }
    public void setEfekAir(int efekAir) {
        this.efekAir = efekAir;
    }

    public void setMentah(boolean mentah) {
        this.mentah = mentah;
    }
    public void setBusuk(boolean busuk) {
        this.busuk = busuk;
    }
    public void setBisaDimasak(boolean bisaDimasak){
        this.bisaDimasak = bisaDimasak;
    }
    public void setKesegaran(int menit){
        this.kesegaran = new Time(menit);
    }
}