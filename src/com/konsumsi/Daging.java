package com.konsumsi;

public class Daging extends Makanan{
    public Daging(){
        setMentah(true);
        setNamaMakanan("Daging Mentah");
        setEfekMakanan(20);
        setKesegaran(720);
        setEfekKesehatan(-15);
        setBisaDimasak(true);
    }
    public void dimasak(){
        setMentah(false);
        setNamaMakanan("Daging Bakar");
        setEfekMakanan(30);
        setKesegaran(2880);
        setEfekKesehatan(0);
        setBisaDimasak(false);
    }

}
