package com.konsumsi;

public class Buah extends Makanan{

    public Buah(String namaTumbuhan, int efekAir, int efekMakanan, int menit, int efekKesehatan){
        super(namaTumbuhan, efekMakanan, efekKesehatan, menit);
        setEfekAir(efekAir);
        setBisaDimasak(false);
    }

}
