package com.crafting;
import com.konsumsi.Makanan;
import com.main.*;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Crafting {
    private  int durasiPengerjaan;
    public Crafting(){}

    // Method Crafting
    public int craftPeralatan(ArrayList<Material> materials, ArrayList<Peralatan> peralatans){
        Scanner input = new Scanner(System.in);
        int i;

        durasiPengerjaan = 0;

        System.out.println("|| PILIH PERALATAN ||");
        System.out.println("1. Pancing | 2 Kayu, 1 Kail, 2 Tali");
        System.out.println("   Dibutuhkan untuk memancing! jangan lupa umpan!");
        System.out.println("2. Bor Busur | 2 Kayu, 1 Tali");
        System.out.println("   Digunakan untuk menyalakan api dengan cepat!");
        System.out.println("3. Tombak Kayu | 3 Kayu, 1 Tali");
        System.out.println("   Tombak dibutuhkan sebelum berburu!");
        System.out.println("0. Kembali");
        System.out.print("Pilih : ");

        try {
            i = input.nextInt();
            System.out.println();
            // Index Material : kayu : 0, tali : 1, kail : 2, umpan : 3, tanaman : 4
            switch (i){
                case 0:
                    break;
                case 1:
                    if(materials.get(0).getKuantitas() < 2 || materials.get(2).getKuantitas() < 1 || materials.get(1).getKuantitas() < 2) {
                        System.out.println("|| Bahan Tidak Cukup ||\n");
                    }
                    else {
                        // kurangi material
                        materials.get(0).kurangiKuantitas(2); // kayu
                        materials.get(2).kurangiKuantitas(1); // kail
                        materials.get(1).kurangiKuantitas(2); // tali

                        Peralatan pancing = new Peralatan("Pancing", 25);
                        peralatans.add(pancing);
                        durasiPengerjaan = 30;

                        System.out.println("|| Berhasil Membuat Pancing ||\n");
                        return durasiPengerjaan;
                    }
                    break;
                case 2:
                    if(materials.get(0).getKuantitas() < 2 || materials.get(1).getKuantitas() < 1)
                        System.out.println("|| Bahan Tidak Cukup ||\n");
                    else{
                        // kurangi material
                        materials.get(0).kurangiKuantitas(2); // kayu
                        materials.get(1).kurangiKuantitas(1); // tali

                        Peralatan borBusur = new Peralatan("Bor Busur", 25);
                        peralatans.add(borBusur);
                        durasiPengerjaan = 60;

                        System.out.println("|| Berhasil Membuat Bor Busur ||\n");
                        return durasiPengerjaan;
                    }
                    break;
                case 3:
                    if(materials.get(0).getKuantitas() < 3 || materials.get(1).getKuantitas() < 1)
                        System.out.println("|| Bahan Tidak Cukup ||\n");
                    else{
                        // kurangi material
                        materials.get(0).kurangiKuantitas(3); // kayu
                        materials.get(1).kurangiKuantitas(1); // tali

                        Peralatan tombak = new Peralatan("Tombak Kayu", 35);
                        peralatans.add(tombak);
                        durasiPengerjaan = 30;

                        System.out.println("|| Berhasil Membuat Tombak Kayu ||\n");
                        return durasiPengerjaan;
                    }
                    break;
            }
        }catch (InputMismatchException e){
            System.out.println("\n|| Terjadi Error Pada Inputan ||\n");
        }
        return durasiPengerjaan;
    }
    public int craftMaterial(ArrayList<Material> materials){
        Scanner input = new Scanner(System.in);
        int i;

        durasiPengerjaan = 0;

        System.out.println("|| PILIH MATERIAL ||");
        System.out.println("1. Tali");
        System.out.println("Butuh : 2 Tanaman Merambat");
        System.out.println("2. Kail Kayu");
        System.out.println("Butuh : 1 Kayu");
        System.out.println("0. Kembali");
        System.out.print("Pilih : ");
        try {
            i = input.nextInt();
            System.out.println();
            switch (i){
                case 0:
                    break;
                case 1:
                    // tanaman rambat index ke 4/ tali index ke 1
                    if(materials.get(4).getKuantitas() < 2)
                        System.out.println("|| Bahan Tidak Cukup ||\n");
                    else{
                        materials.get(4).kurangiKuantitas(2);// kurangi tanaman rambat
                        materials.get(1).tambahKuantitas(1);// tambah tali
                        durasiPengerjaan = 30;

                        System.out.println("|| Berhasil Membuat Tali ||\n");
                        return durasiPengerjaan;
                    }
                    break;
                case 2:
                    // kayu index ke 0/ kail index ke 2
                    if (materials.get(0).getKuantitas() < 1)
                        System.out.println("|| Bahan Tidak Cukup ||\n");
                    else{
                        materials.get(0).kurangiKuantitas(1);// kurangi kayu
                        materials.get(2).tambahKuantitas(1);// tambah kail
                        durasiPengerjaan = 60;

                        System.out.println("|| Berhasil Membuat Kail ||\n");
                        return durasiPengerjaan;
                    }
                    break;
                default:
                    System.out.println("|| Terjadi Error Pada Inputan ||\n");
            }
        }catch (InputMismatchException e){
            System.out.println("\n|| Terjadi Error Pada Inputan ||\n");
        }
        return durasiPengerjaan;
    }
    public int craftUmpan(ArrayList<Makanan> makanans, int i){
        if (i == -1)return 0;
        if (makanans.get(i).getNamaMakanan().contains("Daging")){
            makanans.remove(i);
            System.out.println("|| 3 Umpan didapat ||\n");
            return 3;
        }else {
            makanans.remove(i);
            System.out.println("|| 2 Umpan didapat ||\n");
            return 2;
        }
    }
}
