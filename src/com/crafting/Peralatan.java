package com.crafting;

public class Peralatan {
    private String namaPeralatan;
    private int durabilitas;

    // contructor
    public Peralatan(){}
    public Peralatan(String namaPeralatan, int durabilitas){
        this.namaPeralatan = namaPeralatan;
        this.durabilitas = durabilitas;
    }
    public Peralatan(String namaPeralatan){
        this.namaPeralatan = namaPeralatan;
    }

    public void kurangiDurabilitas(int x){
//        setDurabilitas(getDurabilitas() - x);
        durabilitas -= x;
    }

    // Getter
    public String getNamaPeralatan(){
        return namaPeralatan;
    }
    public int getDurabilitas(){
        return durabilitas;
    }

    // Setter
    public void setNamaPeralatan(String namaPeralatan){
        this.namaPeralatan = namaPeralatan;
    }
    public void setDurabilitas(int durabilitas){
        this.durabilitas = durabilitas;
    }

}
