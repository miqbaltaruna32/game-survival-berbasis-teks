package com.crafting;

public class Material {
    private String namaMaterial;
    private int kuantitas;

    public Material(String namaMaterial){
        this.namaMaterial = namaMaterial;
    }

    // Method
    public void tambahKuantitas(int x){
        kuantitas += x;
    }
    public void kurangiKuantitas(int x){
        kuantitas -= x;
    }

    // Getter
    public String getNamaMaterial(){
        return namaMaterial;
    }
    public int getKuantitas(){
        return kuantitas;
    }

    // Setter
    public void setNamaMaterial(String namaMaterial){
        this.namaMaterial = namaMaterial;
    }
    public void setKuantitas(int kuantitas){
        this.kuantitas = kuantitas;
    }
}
