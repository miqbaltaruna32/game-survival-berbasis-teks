package com.mengumpulkan;

import com.hewan.Ikan;

public class Memancing {
    private int efekEnergi;

    public Memancing(){
        efekEnergi = 0;
    }

    public Ikan memancing(){
        Ikan ikan = new Ikan("Ikan ", 1);
        Ikan ikanBesar = new Ikan("Ikan Besar", 3);

        setEfekEnergi(-2);
        int randomNum = (int)(Math.random() * 100) + 1;
        if(randomNum >= 1 && randomNum <= 30){
            ikan.bunuhHewan();
            return ikan;
        }
        else if(randomNum >= 31 && randomNum <= 50){
            ikanBesar.bunuhHewan();
            return ikanBesar;
        }
        else
            return null;
    }

    // Getter dan Setter
    public int getEfekEnergi() {
        return efekEnergi;
    }
    public void setEfekEnergi(int efekEnergi) {
        this.efekEnergi = efekEnergi;
    }
}
