package com.mengumpulkan;

import com.hewan.Hewan;

public class Berburu {
    private int efekEnergi;

    public Berburu(){
        efekEnergi = 0;
    }

    public Hewan berburu(){
        Hewan tupai = new Hewan("Tupai", 2);
        Hewan rusa = new Hewan("Rusa", 3);

        setEfekEnergi(-4);
        int randomNum = (int)(Math.random() * 100) + 1;
        if(randomNum >= 1 && randomNum <= 15){
            tupai.bunuhHewan();
            return tupai;
        }
        else if(randomNum >= 16 && randomNum <= 25){
            rusa.bunuhHewan();
            return rusa;
        }
        else
            return null;
    }

//    private Hewan bertemuHewan(Hewan hewan){
//        String i = "y";
//        int randomNum = (int)(Math.random() * 100) + 1;
//        System.out.println("|| Kamu Melihat " + hewan.getNamaHewan() + " ||");
//        System.out.print("Serang Atau Tidak (y/t) : ");
//        i = input.nextLine();
//        System.out.println();
//        if(i.equals("y")){
//            if(randomNum >= 1 && randomNum <= 50) {
//                hewan.bunuhHewan();
//                return hewan;
//            } else{
//                System.out.println("|| " + hewan.getNamaHewan() + " Tersebut Kabur ||\n");
//                return null;
//            }
//        } else
//            return null;
//    }

    // Getter dan Setter
    public int getEfekEnergi(){
        return efekEnergi;
    }
    public void setEfekEnergi(int efekEnergi){
        this.efekEnergi = efekEnergi;
    }
}
