package com.mengumpulkan;

import com.konsumsi.*;
import com.crafting.Material;

public class Mengumpulkan {
    private int efekEnergi;

    public Mengumpulkan(){
        efekEnergi = 0;
    }
    // Method
    public int mencariKayu() {
        setEfekEnergi(-2);
        int randomNum = (int)(Math.random() * 100) + 1;
        if(randomNum >= 1 && randomNum <= 35) return 1;
        else if(randomNum >= 36 && randomNum <= 65) return 2;
        else if(randomNum >= 66 && randomNum <= 85) return 3;
        else return 0;
    }

    public Makanan mencariMakanan(){
        Makanan blueberi = new Buah("Blueberi", 5, 5, 1440, 2);
        Makanan kranberi = new Buah("Kranberi", 5, 10, 1440, 1);

        setEfekEnergi(-2);
        int randomNum = (int)(Math.random() * 100) + 1;
        if(randomNum >= 1 && randomNum <= 15) return blueberi;
        else if(randomNum >= 16 && randomNum <= 30) return kranberi;
        else return null;
    }
    public Material mencariMaterial(){
        Material tanamanMerambat = new Material("Tanaman Merambat");

        setEfekEnergi(-5);
        int randomNum = (int)(Math.random() * 100) + 1;
        if(randomNum >= 1 && randomNum <= 30){
            tanamanMerambat.setKuantitas(1);
            return tanamanMerambat;
        }
        else if(randomNum >= 31 && randomNum <= 50){
            tanamanMerambat.setKuantitas(2);
            return tanamanMerambat;
        }
        else if(randomNum >= 51 && randomNum <= 65){
            tanamanMerambat.setKuantitas(3);
            return tanamanMerambat;
        }
        else return null;
    }
    public int getEfekEnergi(){
        return efekEnergi;
    }
    public void setEfekEnergi(int efekEnergi){
        this.efekEnergi = efekEnergi;
    }
}
