package com.hewan;

import com.main.Time;

public class Hewan {
    private String namaHewan;
    private int daging;
    private boolean hidup, busuk;
    private Time kesegaran;

    // constructor
    public Hewan(){}
    public Hewan(String namaHewan, int daging){
        this.namaHewan = namaHewan;
        this.daging = daging;
        this.hidup = true;
    }

    // Method biasa
    public void bunuhHewan(){
        setNamaHewan(getNamaHewan() + " Mati");
        setKesegaran(1440);
        setHidup(false);
    }
    public void pembusukanHewan(){
        setBusuk(true);
        setHidup(false);
        setNamaHewan("Bangkai " + getNamaHewan());
        setDaging(0);
    }
    public void kurangiKesegaran(int waktu){
        kesegaran.kurangiWaktu(waktu);
    }

    // Getter
    public String getNamaHewan(){
        return namaHewan;
    }
    public int getDaging(){
        return daging;
    }
    public boolean isHidup(){
        return hidup;
    }
    public int getKesegaran(){
        return kesegaran.getWaktuUniversal();
    }
    public boolean isBusuk(){
        return busuk;
    }

    // Setter
    public void setNamaHewan(String namaHewan){
        this.namaHewan = namaHewan;
    }
    public void setDaging(int daging){
        this.daging = daging;
    }
    public void setHidup(boolean hidup){
        this.hidup = hidup;
    }
    public void setKesegaran(int waktu){
        kesegaran = new Time(waktu);
    }
    public void setBusuk(boolean busuk){
        this.busuk = busuk;
    }
}
