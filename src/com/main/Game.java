package com.main;

import java.util.ArrayList;

public class Game {
    private Pemain pemain;
    private Time waktuGame;
    private ApiUnggun api;
    private ArrayList<Time> duaWaktu = new ArrayList<>();

    public Game(Time waktu, ApiUnggun api){
        waktuGame = waktu;
        this.api = api;
        duaWaktu.add(waktu);
    }
    public void cetakStat(int waktu){
        manageGame(waktu);
        System.out.println("====================================================");
        waktuGame.cetakWaktuGame();
        System.out.println("|| Kesehatan : " + pemain.getStatKesehatan() + "/100");
        System.out.println("|| Makanan   : " + pemain.getStatMakanan() + "/100");
        System.out.println("|| Air       : " + pemain.getStatAir() + "/100");
        System.out.println("|| Energi    : " + pemain.getStatEnergi() + "/100");
        System.out.println("====================================================");
        System.out.println();
    }
    public int waktuBerlalu(Time waktu){
        Time newWaktu = new Time(waktu.getWaktuUniversal());
        if(duaWaktu.size() == 1){
            duaWaktu.add(1, newWaktu);
        }
        else{
            duaWaktu.remove(0);
            duaWaktu.add(newWaktu);
        }
        return duaWaktu.get(1).getWaktuUniversal() - duaWaktu.get(0).getWaktuUniversal();
    }

    private void manageGame(int waktu){
        pembusukanMakanan(waktu);
        pembusukanHewanBuruan(waktu);
        manageKeadaanPemain(waktu);
        manageStat();
        efekNegatif(waktu);
        manageStat();
        apiUnggunMati(waktu);
        peralatanRusak();
        gameOver();
    }

    private void manageStat(){
        // jika stat kurang dari 0
        if(pemain.getStatKesehatan() < 0)
            pemain.setStatKesehatan(0);
        if(pemain.getStatMakanan() < 0)
            pemain.setStatMakanan(0);
        if(pemain.getStatAir() < 0)
            pemain.setStatAir(0);
        if(pemain.getStatEnergi() < 0)
            pemain.setStatEnergi(0);

        // jika stat melebihi seratus
        if(pemain.getStatKesehatan() > 100)
            pemain.setStatKesehatan(100);
        if(pemain.getStatMakanan() > 100)
            pemain.setStatMakanan(100);
        if(pemain.getStatAir() > 100)
            pemain.setStatAir(100);
        if(pemain.getStatEnergi() > 100)
            pemain.setStatEnergi(100);
    }
    private void gameOver(){
        if(pemain.getStatKesehatan() <= 0) {
            System.out.println("Kesehatan : "  + 0);
            System.out.println("=====KAMU MATI=====");
            System.out.println("  ===GAME OVER===  ");
            System.exit(0);
        }
    }
    private void pembusukanMakanan(int waktu){
        for(int i = 0; i < pemain.getMakananAL().size(); i++){
            pemain.getMakananAL().get(i).kurangiKesegaran(waktu);
            if(pemain.getMakananAL().get(i).getKesegaran() <= 0 && !pemain.getMakananAL().get(i).isBusuk())
                pemain.getMakananAL().get(i).pembusukanMakanan();
        }
    }
    private void pembusukanHewanBuruan(int waktu){
        for(int i = 0; i < pemain.getHewanAL().size(); i++){
            pemain.getHewanAL().get(i).kurangiKesegaran(waktu);
            if(pemain.getHewanAL().get(i).getKesegaran() <= 0 && !pemain.getHewanAL().get(i).isBusuk())
                pemain.getHewanAL().get(i).pembusukanHewan();
        }
    }
    private void manageKeadaanPemain(int waktu){
        int x = waktu/15;
        x *= 2;
        pemain.setStatMakanan(pemain.getStatMakanan() - x);
        pemain.setStatAir(pemain.getStatAir() - x);
    }
    private void efekNegatif(int waktu){
        int x = waktu/30;
        if(pemain.getStatAir() == 0){
            x *= 2;
            pemain.setStatKesehatan(pemain.getStatKesehatan() - x);
        }
        if(pemain.getStatMakanan() == 0){
            pemain.setStatKesehatan(pemain.getStatKesehatan() - x);
        }
    }
    private void apiUnggunMati(int waktu){
        api.kurangiDurasiApi(waktu);
        if(api.getDurasi().getWaktuUniversal() <= 0)
            api.apiMati();
    }
    private void peralatanRusak(){
        for(int i = 0; i < pemain.getPeralatanAL().size(); i++){
            if(pemain.getPeralatanAL().get(i).getDurabilitas() <= 0)
                pemain.getPeralatanAL().remove(i);
        }
    }


    // Setter
    public void setPemain(Pemain pemain) {
        this.pemain = pemain;
    }
    public void setWaktuGame(Time waktuGame) {
        this.waktuGame = waktuGame;
    }
}
