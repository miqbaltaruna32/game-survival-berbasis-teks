package com.main;
// import package
import com.crafting.*;
import com.konsumsi.*;
import com.hewan.*;
import com.mengumpulkan.*;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Pemain {
    private int statMakanan, statAir, statEnergi, statKesehatan;
    private Time time;

    // Inventaris
    private ArrayList<Minuman> minumanAL = new ArrayList<>();
    private ArrayList<Makanan> makananAL = new ArrayList<>();
    private ArrayList<Hewan> hewanAL = new ArrayList<>();
    private ArrayList<Material> materialAL = new ArrayList<>();
    private ArrayList<Peralatan> peralatanAL = new ArrayList<>();

    // Kegiatan
    private Mengumpulkan mengumpulkan = new Mengumpulkan();
    private Berburu berburu = new Berburu();
    private Memancing memancing = new Memancing();
    private Crafting crafting = new Crafting();

    // Constructor
    public Pemain(){
        statMakanan = 60;// 60
        statKesehatan = 70;// 70
        statAir = 50;// 50
        statEnergi = 50;// 50

        persiapanAir();
        persiapanMakanan();
        persiapanMaterial();
    }

    // Method Inventaris
    private void tambahMakanan(Makanan makanan){
        makananAL.add(makanan);
    }
    // Terdapat di Menu Inventaris
    public void buangMakanan(){
        Scanner input = new Scanner(System.in);
        int i;

        System.out.println("|| MEMBUANG MAKANAN ||");
        cetaKInvetarisMakanan();
        System.out.println("0. Kembali");
        try{
            System.out.print("Pilih Makanan : ");
            i = input.nextInt();
            if(i == 0)return;
            i -= 1;
            System.out.println();
            System.out.println("|| Membuang " + makananAL.get(i).getNamaMakanan() + " ||");
            makananAL.remove(i);
        } catch (IndexOutOfBoundsException | InputMismatchException e){
            System.out.println("\n|| Terjadi Error Pada Inputan ||");
        }
    }
    public void buangHewan(){
        Scanner input = new Scanner(System.in);
        int i;

        System.out.println("|| MEMBUANG HEWAN BURUAN ||");
        cetakInvetarisHewan();
        System.out.println("0. Kembali");
        try{
            System.out.print("Pilih Hewan : ");

            i = input.nextInt();
            if(i == 0)return;
            i -= 1;
            System.out.println("|| Membuang " + hewanAL.get(i).getNamaHewan() + " ||");
            hewanAL.remove(i);
        } catch (IndexOutOfBoundsException | InputMismatchException e){
            System.out.println("\n|| Terjadi Error Pada Inputan ||");
        }
    }

    // Method persiapan
    private void persiapanAir(){
        Minuman air1 = new Minuman(true, true);
        Minuman air2 = new Minuman(true, true);
        Minuman air3 = new Minuman(true, true);
        minumanAL.add(air1);
        minumanAL.add(air2);
        minumanAL.add(air3);
    }
    private void persiapanMaterial(){
        Material kayu = new Material("Kayu");
        Material tanamanRambat = new Material("Tanaman Merambat");
        Material tali = new Material("Tali");
        Material kail = new Material("Kail Kayu");
        Material umpan = new Material("Umpan");

        materialAL.add(kayu);
        materialAL.add(tali);
        materialAL.add(kail);
        materialAL.add(umpan);
        materialAL.add(tanamanRambat);
    }
    private void persiapanMakanan(){
        Makanan makananKaleng = new Makanan("Makanan Kaleng", 35, 0, 500000);
        Makanan makananKaleng1 = new Makanan("Makanan Kaleng", 35, 0, 500000);

        tambahMakanan(makananKaleng);
        tambahMakanan(makananKaleng1);
    }

    // Method Konsumsi
    public void minum(){
        if(statAir >= 100)
            System.out.println("|| Kamu Tidak Haus ||\n");
        else{
            Scanner input = new Scanner(System.in);
            int i;

            System.out.println("|| MINUM ||");
            cetaKInvetarisAir();
            System.out.println("0. Kembali");
            System.out.print("Pilih Minuman : ");
            try {
                i = input.nextInt();
                System.out.println();
                if (i == 0)return;
                i -= 1;
                String namaMinuman = minumanAL.get(i).getNamaMinuman();
                if (namaMinuman.equals("Air Botol(Kosong)"))
                    System.out.println("||Botol Kosong||\n");
                else {
                    System.out.println("|| Minum Air ||");
                    statAir += minumanAL.get(i).getEfekAir();
                    statKesehatan += minumanAL.get(i).getEfekKesehatan();
                    minumanAL.get(i).kosongkanBotol();

                    System.out.println();
                }
            } catch (IndexOutOfBoundsException | InputMismatchException e) {
                System.out.println("|| Terjadi Error Pada Inputan ||\n");
            }
        }
        if(statAir > 100) statAir = 100;
    }
    public void makan(){
        if(statMakanan >= 100)
            System.out.println("|| Kamu Tidak Lapar ||\n");
        else{
            Scanner input = new Scanner(System.in);
            int i;

            System.out.println("|| MAKAN ||");
            cetaKInvetarisMakanan();
            System.out.println("0. Kembali");
            System.out.print("Pilih Makanan : ");

            try {
                i = input.nextInt();
                System.out.println();
                if (i == 0)return;
                i -= 1;
                String namaMakanan = makananAL.get(i).getNamaMakanan();
                System.out.println("||Makan " + namaMakanan + "||");
                statMakanan += makananAL.get(i).getEfekMakanan();
                statKesehatan += makananAL.get(i).getEfekKesehatan();
                statAir += makananAL.get(i).getEfekAir();
                makananAL.remove(i);
            } catch (IndexOutOfBoundsException | InputMismatchException e) {
                System.out.println("|| Terjadi Error Pada Inputan ||\n");
            }
        }

        System.out.println();
        if(statMakanan > 100) statMakanan = 100;
        if(statMakanan < 0) statMakanan = 0;
        if(statAir < 0) statAir = 0;
    }

    // Method Kegiatan
    public void potongHewan(){
        Scanner input = new Scanner(System.in);
        int i;

        System.out.println("|| MEMOTONG HEWAN ||");
        if(hewanAL.size() == 0)
            System.out.println("|| Tidak Ada Hasil Buruan ||\n");
        else{
            cetakInvetarisHewan();
            System.out.println("0. Kembali");
            System.out.print("Pilih Hewan : ");
            try {
                i = input.nextInt();
                if (i == 0)return;
                i -= 1;
                System.out.println();
                if(!hewanAL.get(i).isBusuk()){
                    for (int j = 0; j < hewanAL.get(i).getDaging(); j++){
                        Daging daging = new Daging();
                        tambahMakanan(daging);
                    }

                    hewanAL.remove(i);
                } else System.out.println("|| Hewan Sudah Membusuk ||\n");
            } catch (IndexOutOfBoundsException | InputMismatchException e){
                System.out.println("\n|| Terjadi Error Pada Inputan ||\n");
            }
        }
    }
    public void mencariKayu(){
        System.out.println("|| MENCARI KAYU ||");
        int kuantitas = mengumpulkan.mencariKayu();
        System.out.println("|| Kamu mendapatkan " + kuantitas + " kayu ||\n");
        kuantitas += materialAL.get(0).getKuantitas();
        materialAL.get(0).setKuantitas(kuantitas);
        time.tambahWaktu(30);
        statEnergi += mengumpulkan.getEfekEnergi();
    }
    public void mengisiBotolAir(){
        int randommNum = (int) (Math.random() * 100) + 1;
        System.out.println("|| MENGISI AIR ||");
        if(randommNum <= 20){
            System.out.println("|| Menemukan Sumber Air ||");
            for(Minuman air : minumanAL){
                if (!air.isPenuh()){
                    air.isiBotol();
                    System.out.println("|| Mengisi Botol Air ||");
                }
            }
            System.out.println();
        }else
            System.out.println("|| Tidak Bisa Menemukan Sumber Air ||\n");
        time.tambahWaktu(15);
        statEnergi -= 2;
    }
    public void mencariMakanan(){
        System.out.println("|| MENCARI MAKANAN ||");
        Makanan newMakanan = mengumpulkan.mencariMakanan();
        if(newMakanan == null)
            System.out.println("|| Tidak Ada Yang Bisa Ditemukan ||\n");
        else {
            System.out.println("|| Menemukan " + newMakanan.getNamaMakanan() + " Liar ||\n");
            makananAL.add(newMakanan);
        }
        time.tambahWaktu(30);
        statEnergi += mengumpulkan.getEfekEnergi();
    }
    public void mencariMaterial(){
        System.out.println("|| MENCARI MATERIAL ||");
        Material newMaterial = mengumpulkan.mencariMaterial();
        int kuantitas, i;
        if(newMaterial == null)
            System.out.println("|| Tidak Ada Yang Bisa Ditemukan ||\n");
        else{
            System.out.println("|| Menemukan " + newMaterial.getKuantitas() + " " + newMaterial.getNamaMaterial() + " ||\n");

            kuantitas = materialAL.get(4).getKuantitas();
            materialAL.get(4).setKuantitas(kuantitas + newMaterial.getKuantitas());
        }
        time.tambahWaktu(30);
        statEnergi += mengumpulkan.getEfekEnergi();
    }
    public void berburu(){
        boolean adaSenjata = false;
        int indexTombak = 0;

        System.out.println("|| BERBURU ||");
        // cek apakah sudah buat senjata
        for (Peralatan peralatan : peralatanAL) {
            if (peralatan.getNamaPeralatan().contains("Tombak")) {
                adaSenjata = true;
                break;
            }
            indexTombak++;
        }
        if(adaSenjata) {
            Hewan hewanBuruan = berburu.berburu();
            if (hewanBuruan == null)
                System.out.println("|| Gagal Berburu ||\n");
            else {
                System.out.println("|| Kamu Mendapatkan " + hewanBuruan.getNamaHewan() + " ||\n");
                peralatanAL.get(indexTombak).kurangiDurabilitas(1);
                hewanAL.add(hewanBuruan);
            }
            time.tambahWaktu(60);
            statEnergi += berburu.getEfekEnergi();
        }else
            System.out.println("|| Kamu Tidak Memiliki Senjata ||\n");
    }
    public void memancing(){
        boolean adaSenjata = false;
        int indexPancing = 0;

        System.out.println("|| MEMANCING ||");
        // cek apakah sudah buat pancing
        for (Peralatan peralatan : peralatanAL) {
            if (peralatan.getNamaPeralatan().contains("Pancing")) {
                adaSenjata = true;
                break;
            }
            indexPancing++;
        }
        if(adaSenjata && materialAL.get(3).getKuantitas() >= 1) {
            Ikan hasilPancing = memancing.memancing();
            if (hasilPancing == null) {
                System.out.println("|| Gagal Memancing ||\n");
            } else {
                System.out.println("|| Kamu Mendapatkan " + hasilPancing.getNamaHewan() + " ||\n");
                materialAL.get(3).kurangiKuantitas(1);
                hewanAL.add(hasilPancing);
                peralatanAL.get(indexPancing).kurangiDurabilitas(1);
            }
            time.tambahWaktu(60);
            statEnergi += berburu.getEfekEnergi();
        }else
            System.out.println("|| Kamu Tidak Memiliki Pancing Atau Umpan ||\n");
    }
    public void menyalakanApi(ApiUnggun api){
        boolean adaAlat = false;
        int indexAlat = 0;

        System.out.println("|| MENYALAKAN API UNGGUN ||");
        // cek apakah sudah buat alat
        for (Peralatan peralatan : peralatanAL) {
            if (peralatan.getNamaPeralatan().contains("Bor")) {
                adaAlat = true;
                break;
            }
            indexAlat++;
        }
        if(adaAlat && materialAL.get(0).getKuantitas() >= 1){
            if(api.isMenyala()){
                System.out.println("|| Api Unggun Sudah Menyala ||\n");
            }
            System.out.println("|| Kamu Berhasil Menyalakan Api Unggun ||");
            api.apiMenyala();
            api.cekApiUnggun();

            time.tambahWaktu(15);
            statEnergi += -5;
        }else
            System.out.println("|| Kamu Tidak Memiliki Alat atau Kayu ||\n");
    }
    public void tambahKayu(ApiUnggun api){
        Scanner input = new Scanner(System.in);
        int i = 0;

        if(api.isMenyala()){
            System.out.println("|| MENAMBAH KAYU KE API UNGGUN ||");
            System.out.println("Kayu => " + materialAL.get(0).getKuantitas());
            System.out.print("Jumlah Kayu : ");
            try {
                i = input.nextInt();
                System.out.println();
                if(i > materialAL.get(0).getKuantitas())
                    System.out.println("|| Kayu Tidak Cukup ||\n");
                else{
                    materialAL.get(0).kurangiKuantitas(i);
                    api.tambahDurasiApi(i);
                    api.cekApiUnggun();
                }
            }catch (InputMismatchException e){
                System.out.println("|| Terjadi Error Pada Inputan ||\n");
            }
        }else
            System.out.println("|| Api Belum Menyala ||\n");

    }
    public void memasak(ApiUnggun api){
        System.out.println("|| MEMASAK ||");
        if(!api.isMenyala())
            System.out.println("|| Tidak Ada Sumber Api ||\n");
        else {
            cetaKInvetarisMakanan();
            Scanner input = new Scanner(System.in);
            int i;
            System.out.print("Pilih Makanan : ");

            try{
                i = input.nextInt();
                i -= 1;
                System.out.println();
                System.out.println("|| MEMASAK ||");
                if(makananAL.get(i).isBisaDimasak()){
                    makananAL.get(i).dimasak();
                }else
                    System.out.println("|| Makanan Tersebut Tidak Bisa Dimasak ||\n");
            }catch (IndexOutOfBoundsException | InputMismatchException e) {
                System.out.println("|| Terjadi Error Pada Inputan ||\n");
            }
        }
    }
    public void merebusAir(ApiUnggun api){
        System.out.println("|| MEREBUS AIR ||");
        if (!api.isMenyala())
            System.out.println("|| Tidak Ada Sumber Api ||\n");
        else{
            for (Minuman minuman : minumanAL) {
                if (minuman.isPenuh() && !minuman.isAman()) {
                    minuman.diRebus();
                }
            }
        }
    }
    public void istirahat(){
        Scanner input = new Scanner(System.in);
        int i;

        System.out.println("|| ISTIRAHAT ||");
        System.out.println("1. Istirahat 1 jam");
        System.out.println("2. Istirahat 4 jam");
        System.out.println("0. Kembali");
        System.out.print("Pilih : ");
        try{
            i = input.nextInt();
            System.out.println();
            if(i == 0)return;
            switch (i){
                case 1:
                    System.out.println("|| Kamu Istirahat Selama 1 Jam ||\n");
                    time.tambahWaktu(60);
                    statKesehatan += 1;
                    statEnergi += 15;
                    break;
                case 2:
                    System.out.println("|| Kamu Istirahat Selamat 4 Jam ||\n");
                    time.tambahWaktu(240);
                    statKesehatan += 4;
                    statEnergi += 50;
                    break;
                default:
            }
        } catch (InputMismatchException e){
            System.out.println("|| Terjadi Error Pada Inputan ||\n");
        }
    }

    // Method Crafting
    public void craftMaterial(){
        System.out.println("|| CRAFT MATERIAL ||");
        cetakInventarisMaterial();
        int durasiPengerjaan = crafting.craftMaterial(materialAL);
        time.tambahWaktu(durasiPengerjaan);
    }
    public void craftUmpan(){
        Scanner input = new Scanner(System.in);
        int i;

        System.out.println("|| CRAFT UMPAN ||");
        cetaKInvetarisMakanan();
        System.out.println("0. Kembali");
        System.out.print("Pilih Bahan : ");
        i = input.nextInt();
        i -= 1;
        System.out.println();

        int banyakUmpan = crafting.craftUmpan(makananAL, i);
        materialAL.get(3).tambahKuantitas(banyakUmpan);
    }
    public void craftPeralatan(){
        System.out.println("|| CRAFT PERALATAN ||");
        cetakInventarisMaterial();
        int durasiPengerjaan = crafting.craftPeralatan(materialAL, peralatanAL);
        time.tambahWaktu(durasiPengerjaan);
    }

    // Method Cetak

    public void cetaKInvetarisAir(){
        System.out.println("|| Inventaris Minuman ||");
        for (int i = 0; i < minumanAL.size(); i++)
            System.out.println(i + 1 + ". " + minumanAL.get(i).getNamaMinuman());
    }
    public void cetaKInvetarisMakanan(){
        System.out.println("|| Inventaris Makanan ||");
        if(makananAL.size() == 0)
            System.out.println("| Inventaris Kosong |");
        else{
            for (int i = 0; i < makananAL.size(); i++)
                System.out.println(i + 1 + ". " + makananAL.get(i).getNamaMakanan());
        }
    }
    public void cetakInvetarisHewan(){
        System.out.println("|| Inventaris Hewan ||");
        if(hewanAL.size() == 0)
            System.out.println("| Inventaris Kosong |");
        else{
            for (int i = 0; i < hewanAL.size(); i++)
                System.out.println(i + 1 + ". " + hewanAL.get(i).getNamaHewan());
        }
    }
    public void cetakInventarisMaterial(){
        System.out.println("|| Inventaris Material ||");
        for (Material material : materialAL) {
            if (material.getKuantitas() <= 0) continue;
            System.out.print("   " + material.getNamaMaterial());
            System.out.println(" => " + material.getKuantitas());
        }
    }
    public void cetakInventarisPeralatan(){
        System.out.println("|| Inventaris Peralatan ||");
        if(peralatanAL.size() == 0)
            System.out.println("| Invetaris Kosong |");
        else{
            for (int i = 0; i < peralatanAL.size(); i++){
                System.out.println(i + 1 +  ". " + peralatanAL.get(i).getNamaPeralatan());
                System.out.println("Durabilitas : " + peralatanAL.get(i).getDurabilitas());
                System.out.println();
            }
        }
    }

    // Getter
    public int getStatMakanan() {
        return statMakanan;
    }
    public int getStatAir() {
        return statAir;
    }
    public int getStatEnergi() {
        return statEnergi;
    }
    public int getStatKesehatan() {
        return statKesehatan;
    }
    public Time getTime() {
        return time;
    }
    public ArrayList<Makanan> getMakananAL() {
        return makananAL;
    }
    public ArrayList<Hewan> getHewanAL() {
        return hewanAL;
    }
    public ArrayList<Peralatan> getPeralatanAL() {
        return peralatanAL;
    }

    // Setter
    public void setStatMakanan(int statMakanan) {
        this.statMakanan = statMakanan;
    }
    public void setStatAir(int statAir) {
        this.statAir = statAir;
    }
    public void setStatEnergi(int statEnergi) {
        this.statEnergi = statEnergi;
    }
    public void setStatKesehatan(int statKesehatan) {
        this.statKesehatan = statKesehatan;
    }
    public void setWaktu(Time time){
        this.time = time;
    }
}