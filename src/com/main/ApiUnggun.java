package com.main;

public class ApiUnggun {
    private boolean menyala;
    private Time durasi;

    public ApiUnggun(){
        menyala = false;
        durasi = new Time(0);
    }

    public void cekApiUnggun(){
        if (menyala){
            System.out.println("|| Api Unggun Menyala ||");
            System.out.print("   Durasi : ");
            durasi.cetakWaktu();
            System.out.println();
        }else
            System.out.println("|| Api Unggun Tidak Menyala ||\n");
    }
    public void apiMenyala(){
        menyala = true;
        durasi.setWaktu(60);
    }
    public void apiMati(){
        menyala = false;
        durasi.setWaktu(0);
    }
    public void tambahDurasiApi(int i){
        i *= 60;
        durasi.tambahWaktu(i);
    }
    public void kurangiDurasiApi(int waktu){
        durasi.kurangiWaktu(waktu);
    }
    // Getter
    public boolean isMenyala() {
        return menyala;
    }
    public Time getDurasi() {
        return durasi;
    }

    // Setter
    public void setMenyala(boolean menyala) {
        this.menyala = menyala;
    }
    public void setDurasi(Time durasi) {
        this.durasi = durasi;
    }
}
