package com.main;

public class Time {
    private int waktuUniversal; // Dalam Bentuk Menit

    public Time(int waktuUniversal){
        this.waktuUniversal = waktuUniversal;
    }

    public int getHari(){
        return waktuUniversal / 1440;
    }
    public int getJam(){
        if (waktuUniversal > 1440){
            return (waktuUniversal % 1440) / 60;
        }else {
            return waktuUniversal/60;
        }
    }
    public int getMenit(){
        double temp;
        if(waktuUniversal > 1440){
            temp = (waktuUniversal % 1440) * 100 / 60;
        }else {
            temp = waktuUniversal * 100/60;
        }
        double menit = (temp - getJam() * 100);

        if(menit == 25) menit = 15;
        else if (menit == 50) menit= 30;
        else if (menit == 75) menit = 45;

        return (int) menit;
    }
    public void tambahWaktu(int menit){
        waktuUniversal += menit;
    }
    public void kurangiWaktu(int menit){
        waktuUniversal -= menit;
    }
    public void cetakWaktu(){
        String stringJam, stringMenit;
        if(getJam() < 10) stringJam = "0" + getJam();
        else stringJam = "" + getJam();

        if(getMenit() < 10) stringMenit = "0" + getMenit();
        else stringMenit = "" + getMenit();

        System.out.printf("|| %s:%s\n", stringJam,stringMenit);
    }
    public void cetakWaktuGame(){
        System.out.println("|| Hari ke-" + getHari());
        cetakWaktu();
    }

    public int getWaktuUniversal() {
        return waktuUniversal;
    }

    public void setWaktu(int menit){
        waktuUniversal = menit;
    }
}
