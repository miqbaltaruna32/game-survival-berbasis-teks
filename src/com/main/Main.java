package com.main;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        // ===Persiapan Awal Game===
        Time waktuGame = new Time(1920);
        Pemain p1 = new Pemain();
        ApiUnggun api = new ApiUnggun();
        Game game = new Game(waktuGame, api);
        int i;

        p1.setWaktu(waktuGame);
        game.setPemain(p1);
        // ===Persiapan Awal Game===
        while (true){
            System.out.println("=====================================================================================");
            game.cetakStat(game.waktuBerlalu(waktuGame));
            System.out.println("===MENU UTAMA===");
            System.out.println("1. Konsumsi");
            System.out.println("2. Berburu");
            System.out.println("3. Mengumpulkan");
            System.out.println("4. Inventaris");
            System.out.println("5. Api Unggun");
            System.out.println("6. Crafting");
            System.out.println("7. Istirahat");
            System.out.println("0. Keluar");

            try {
                i = pilihan();
                switch (i){
                    case 0:
                        System.out.println("|| Keluar Dari Game ||");
                        System.out.println("1. Ya");
                        System.out.println("0. Tidak");
                        int x = pilihan();
                        if(x == 1)
                            System.exit(0);
                        break;
                    case 1:
                        konsumsi(p1);
                        break;
                    case 2:
                        if(p1.getStatEnergi() <= 5)
                            System.out.println("|| Kamu Terlalu Lelah ||\n");
                        else
                            berburu(p1);
                        break;
                    case 3:
                        if (p1.getStatEnergi() <= 5)
                            System.out.println("|| Kamu Terlalu Lelah ||\n");
                        else
                            mengumpulkan(p1);
                        break;
                    case 4:
                        inventaris(p1);
                        break;
                    case 5:
                        apiUnggun(p1, api);
                        break;
                    case 6:
                        crafting(p1);
                        break;
                    case 7:
                        if(p1.getStatEnergi() == 100)
                            System.out.println("|| Kamu Tidak Lelah ||\n");
                        else
                            p1.istirahat();
                        break;
                    default:
                }
            }catch (InputMismatchException e){
                System.out.println("|| Terjadi Error Pada Inputan ||\n");
            }
        }
    }
    private static int pilihan(){
        Scanner input = new Scanner(System.in);
        int i;

        System.out.print("Pilih : ");
        try{
            i = input.nextInt();
            System.out.println();
            return i;
        }catch (InputMismatchException e){
            System.out.println("|| Terjadi Error Pada Inputan ||\n");
        }
        return 99;
    }
    private static void konsumsi(Pemain pemain){
        while(true){
            System.out.println("==MENU KONSUMSI==");
            System.out.println("1. Makan");
            System.out.println("2. Minum");
            System.out.println("3. Potong Hewan");
            System.out.println("4. Mengisi Botol Air");
            System.out.println("0. Kembali");
            try{
                int i = pilihan();
                switch (i){
                    case 0:
                        return;
                    case 1:
                        pemain.makan();
                        break;
                    case 2:
                        pemain.minum();
                        break;
                    case 3:
                        pemain.potongHewan();
                        break;
                    case 4:
                        pemain.mengisiBotolAir();
                        break;
                    default:
                }
            }catch (InputMismatchException e){
                System.out.println("|| Terjadi Error Pada Inputan ||\n");
            }
        }
    }
    private static void berburu(Pemain pemain){
        System.out.println("==MENU BERBURU==");
        System.out.println("1. Berburu");
        System.out.println("2. Memancing");
        System.out.println("0. Kembali");
        try{
            int i = pilihan();
            switch (i){
                case 1:
                    pemain.berburu();
                    break;
                case 2:
                    pemain.memancing();
                    break;
                default:
            }
        }catch (InputMismatchException e){
            System.out.println("|| Terjadi Error Pada Inputan ||\n");
        }
    }
    private static void mengumpulkan(Pemain pemain){
        System.out.println("|| MENU MENGUMPULKAN ||");
        System.out.println("1. Mencari Kayu");
        System.out.println("2. Mencari Makanan");
        System.out.println("3. Mencari Material");
        System.out.println("0. Kembali");

        try {
            int i = pilihan();
            switch (i){
                case 1:
                    pemain.mencariKayu();
                    break;
                case 2:
                    pemain.mencariMakanan();
                    break;
                case 3:
                    pemain.mencariMaterial();
                    break;
                default:
            }
        }catch (InputMismatchException e){
            System.out.println("|| Terjadi Error Pada Inputan ||\n");
        }
    }
    private static void inventaris(Pemain pemain){
        while(true){
            System.out.println("\n==MENU INVENTARIS==");
            System.out.println("1. Makanan");
            System.out.println("2. Minuman");
            System.out.println("3. Hewan Buruan/Ikan");
            System.out.println("4. Material");
            System.out.println("5. Peralatan");
            System.out.println("6. Buang Makanan");
            System.out.println("7. Buang Hewan Buruan");
            System.out.println("0. Kembali");
            try{
                int i = pilihan();

                switch (i){
                    case 0:
                        return;
                    case 1:
                        pemain.cetaKInvetarisMakanan();
                        break;
                    case 2:
                        pemain.cetaKInvetarisAir();
                        break;
                    case 3:
                        pemain.cetakInvetarisHewan();
                        break;
                    case 4:
                        pemain.cetakInventarisMaterial();
                        break;
                    case 5:
                        pemain.cetakInventarisPeralatan();
                        break;
                    case 6:
                        pemain.buangMakanan();
                        break;
                    case 7:
                        pemain.buangHewan();
                    default:
                }
            }catch (InputMismatchException e){
                System.out.println("|| Terjadi Error Pada Inputan ||\n");
            }
        }
    }
    private static void apiUnggun(Pemain pemain, ApiUnggun api){
        while (true){
            System.out.println("==MENU API UNGGUN==");
            api.cekApiUnggun();
            System.out.println("1. Menyalakan Api");
            System.out.println("2. Tambah Kayu");
            System.out.println("3. Memasak");
            System.out.println("4. Merebus Air");
            System.out.println("0. Kembali");

            try {
                int i = pilihan();

                switch (i){
                    case 0:
                        return;
                    case 1:
                        pemain.menyalakanApi(api);
                        break;
                    case 2:
                        pemain.tambahKayu(api);
                        break;
                    case 3:
                        pemain.memasak(api);
                        break;
                    case 4:
                        pemain.merebusAir(api);
                        break;
                    default:
                }
            }catch (InputMismatchException e){
                System.out.println("|| Terjadi Error Pada Inputan ||\n");
            }
        }
    }
    private static void crafting(Pemain pemain){
        while (true){
            System.out.println("==MENU CRAFTING==");
            System.out.println("1. Craft Material");
            System.out.println("2. Craft Peralatan");
            System.out.println("3. Buat Umpan");
            System.out.println("0. Kembali");

            try {
                int i = pilihan();

                switch (i){
                    case 0:
                        return;
                    case 1:
                        pemain.craftMaterial();
                        break;
                    case 2:
                        pemain.craftPeralatan();
                        break;
                    case 3:
                        pemain.craftUmpan();
                        break;
                    default:
                }
            }catch (InputMismatchException e){
                System.out.println("|| Terjadi Error Pada Inputan ||\n");
            }
        }
    }
}
